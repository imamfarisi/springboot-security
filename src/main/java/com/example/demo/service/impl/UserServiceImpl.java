package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demo.model.User;
import com.example.demo.repo.UserRepo;
import com.example.demo.service.UserService;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@Service
public class UserServiceImpl implements UserService {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	private UserRepo repo;

	@Autowired
	private PasswordEncoder encoder;

	@Override
	public User insert(final User data) {
		data.setPass(encoder.encode(data.getPass()));
		return repo.save(data);
	}

	@Override
	public User update(final User data) {
		final User userDb = repo.findById(data.getId()).get();
		em.detach(userDb);

		userDb.setPass(encoder.encode(data.getPass()));
		userDb.setVersion(data.getVersion());

		return repo.saveAndFlush(userDb);
	}

	@Override
	public List<User> getAll() {
		return repo.findAll();
	}

	@Override
	public Optional<User> getById(final Long id) {
		return repo.findById(id);
	}

	@Override
	public Optional<User> getByEmail(final String email) {
		return repo.findByEmail(email);
	}

	@Override
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
		final Optional<User> userOptional = repo.findByEmail(username);

		if (userOptional.isPresent()) {
			return new org.springframework.security.core.userdetails.User(username, userOptional.get().getPass(),
					new ArrayList<>());
		}

		throw new UsernameNotFoundException("Wrong email and password");
	}

}
