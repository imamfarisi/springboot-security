package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.example.demo.model.User;

public interface UserService extends UserDetailsService {

	User insert(User data);

	User update(User data);

	List<User> getAll();

	Optional<User> getById(Long id);

	Optional<User> getByEmail(String email);

}
