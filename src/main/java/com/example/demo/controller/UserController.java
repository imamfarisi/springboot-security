package com.example.demo.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.LoginReqDto;
import com.example.demo.dto.LoginResDto;
import com.example.demo.model.User;
import com.example.demo.service.JwtService;
import com.example.demo.service.UserService;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@RestController
@RequestMapping("users")
@SecurityRequirement(name = "bearerAuth")
public class UserController {

	private final UserService userService;
	private final AuthenticationManager authenticationManager;
	private final JwtService jwtService;

	public UserController(UserService userService, AuthenticationManager authenticationManager, JwtService jwtService) {
		this.userService = userService;
		this.authenticationManager = authenticationManager;
		this.jwtService = jwtService;
	}

	@PostMapping("login")
	public ResponseEntity<?> login(@RequestBody final LoginReqDto user) {
		final Authentication auth = new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPass());

		authenticationManager.authenticate(auth);
		final Optional<User> userOptional = userService.getByEmail(user.getEmail());

		final Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.HOUR_OF_DAY, 1);

		final Map<String, Object> claims = new HashMap<>();
		claims.put("exp", cal.getTime());
		claims.put("id", userOptional.get().getId());

		final LoginResDto loginRes = new LoginResDto();
		loginRes.setToken(jwtService.generateJwt(claims));

		return new ResponseEntity<>(loginRes, HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<?> insert(@RequestBody final User user) {
		final User userInsert = userService.insert(user);
		return new ResponseEntity<>(userInsert, HttpStatus.CREATED);
	}

	@PutMapping
	public ResponseEntity<?> update(@RequestBody final User user) {
		final User userInsert = userService.update(user);
		return new ResponseEntity<>(userInsert, HttpStatus.OK);
	}

	@GetMapping
	public ResponseEntity<?> getAll() {
		final List<User> users = userService.getAll();
		return new ResponseEntity<>(users, HttpStatus.OK);
	}
}
